import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final player = AudioCache();

  void playSound(n) {
    player.play('note$n.wav');
  }

  Expanded buttonBar({Color color, int num}) {
    return Expanded(
      // ignore: deprecated_member_use
      child: RaisedButton(
        color: color,
        onPressed: () {
          playSound(num);
        },
        child: null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              buttonBar(color: Colors.red, num: 1),
              buttonBar(color: Colors.orange, num: 2),
              buttonBar(color: Colors.green, num: 3),
              buttonBar(color: Colors.pink, num: 4),
              buttonBar(color: Colors.yellow, num: 5),
              buttonBar(color: Colors.blue, num: 6),
              buttonBar(color: Colors.indigo, num: 7),
            ],
          ),
        ),
      ),
    );
  }
}
